﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Collections.Generic;


namespace WebAppTest.Data
{
    public class Book
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string Isbn { get; set; }
        public string PublisheName { get; set; }
        public string AuthorName { get; set; }
        public string CategoryName { get; set; }

        public List<Book> GetBooks(string ConnectionString)
        {
            List<Book> bookList = new List<Book>();

            SqlConnection cn = new SqlConnection(ConnectionString);

            string selectSQL = "select BookId, Title, ISBN, PublisherName, AuthorName, CategoryName from GetBookData";

            cn.Open();

            SqlCommand cmd = new SqlCommand(selectSQL, cn);

            SqlDataReader dr = cmd.ExecuteReader();

            if(dr != null)
            {
                while(dr.Read())
                {
                    Book book = new Book();
                    book.BookId = Convert.ToInt32(dr["BookId"]);
                    book.Title = dr["Title"].ToString();
                    book.Isbn = dr["ISBN"].ToString();
                    book.PublisheName = dr["PublisherName"].ToString();
                    book.AuthorName = dr["AuthorName"].ToString();
                    book.CategoryName = dr["CategoryName"].ToString();

                    bookList.Add(book);
                }

            }
            return bookList;
        }

        public void CreateBook(string connectionString, Book book)
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("CreateBook", cn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Title",Title));
                    cmd.Parameters.Add(new SqlParameter("@Isbn", Isbn));
                    cmd.Parameters.Add(new SqlParameter("@PublisherName", PublisheName));
                    cmd.Parameters.Add(new SqlParameter("@AuthorName", AuthorName));
                    cmd.Parameters.Add(new SqlParameter("@CategoryName", CategoryName));
                    cn.Open();
                    cmd.ExecuteReader();
                    cn.Close();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public Book GetBookData(string connectionString, int bookId)
        {
            SqlConnection con = new SqlConnection(connectionString);
            string selectSQL = "select BookId, Title, Isbn, PublisherName, AuthorName, CategoryName from GetBookData where BookId = " + bookId;
            con.Open();
            SqlCommand cmd = new SqlCommand(selectSQL, con);
            SqlDataReader dr = cmd.ExecuteReader();
            Book book = new Book();
            if (dr != null)
            {
                while (dr.Read())
                {
                    book.BookId = Convert.ToInt32(dr["BookId"]);
                    book.Title = dr["Title"].ToString();
                    book.Isbn = dr["ISBN"].ToString();
                    book.PublisheName = dr["PublisherName"].ToString();
                    book.AuthorName = dr["AuthorName"].ToString();
                    book.CategoryName = dr["CategoryName"].ToString();
                }
            }
            return book;
        }

        public void EditBook(string connectionString, Book book)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand("UpdateBook", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@BookId", book.BookId));
                    cmd.Parameters.Add(new SqlParameter("@Title", book.Title));
                    cmd.Parameters.Add(new SqlParameter("@Isbn", book.Isbn));
                    cmd.Parameters.Add(new SqlParameter("@PublisherName", book.PublisheName));
                    cmd.Parameters.Add(new SqlParameter("@AuthorName", book.AuthorName));
                    cmd.Parameters.Add(new SqlParameter("@CategoryName", book.CategoryName));
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}