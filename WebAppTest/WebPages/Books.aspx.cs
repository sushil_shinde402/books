﻿using System;
using System.Web.Configuration;
using WebAppTest.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace WebAppTest.WebPages
{

    public partial class Books : System.Web.UI.Page
    {
        private string connectionString = WebConfigurationManager.ConnectionStrings["LibraryConnectionString"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                FillBookGrid();
            }
        }

        private void FillBookGrid()
        {
            List<Book> bookList = new List<Book>();
            Book book = new Book();
            bookList = book.GetBooks(connectionString);

            gridBookList.DataSource = bookList;
            gridBookList.DataBind();
        }

        protected void btnNewBook_Click(object sender, EventArgs e)
        {
            Response.Redirect("NewBook.aspx");
        }

        protected void btnEditBook_Click(object sender, EventArgs e)
        {
            string bookId = null;
            if (gridBookList.SelectedIndex != -1)
                bookId = gridBookList.SelectedRow.Cells[0].Text;
            Response.Redirect("EditBook.aspx?bookId="+ bookId);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            foreach (GridViewRow row in gridBookList.Rows)
            {
                row.Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference
                (gridBookList, "Select$" + row.RowIndex.ToString(), true));
            }
            base.Render(writer);
        }
        protected void gridBookList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("OnMouseOver", "this.style.cursor='pointer';");
                e.Row.ToolTip = "Click on select row";
            }
        }
    }
}