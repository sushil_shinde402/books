﻿using System;
using WebAppTest.Data;
using System.Data.SqlClient;
using System.Data;
using System.Web.Configuration;

namespace WebAppTest.WebPages
{
    public partial class NewBook : System.Web.UI.Page
    {
        private string connectionString = WebConfigurationManager.ConnectionStrings["LibraryConnectionString"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Books.aspx");
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            Book book = new Book();
            book.Title = txtTitle.Text;
            book.Isbn = txtIsbn.Text;
            book.PublisheName = txtPublisher.Text;
            book.AuthorName = txtAuthor.Text;
            book.CategoryName = txtCategory.Text;
            book.CreateBook(connectionString, book);
            Response.Redirect("Books.aspx");

        }
    }
}