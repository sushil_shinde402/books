﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewBook.aspx.cs" Inherits="WebAppTest.WebPages.NewBook" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>New Books</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>New Book</h1>
            <asp:Label ID="lblTitle" runat="server" Text="Title:"></asp:Label>
            <br />
            <asp:TextBox ID="txtTitle" runat="server" Width="300px"></asp:TextBox>
            <br />
            <asp:Label ID="lblIsbn" runat="server" Text="ISBN:"></asp:Label>
            <br />
            <asp:TextBox ID="txtIsbn" runat="server" Width="300px"></asp:TextBox>
            <br />
            <asp:Label ID="lblPublisher" runat="server" Text="Publisher:"></asp:Label>
            <br />
            <asp:TextBox ID="txtPublisher" runat="server" Width="300px"></asp:TextBox>
            <br />
            <asp:Label ID="lblAuthor" runat="server" Text="Author:"></asp:Label>
            <br />
            <asp:TextBox ID="txtAuthor" runat="server" Width="300px"></asp:TextBox>
            <br />
            <asp:Label ID="lblCategory" runat="server" Text="Category:"></asp:Label>
            <br />
            <asp:TextBox ID="txtCategory" runat="server" Width="300px"></asp:TextBox>
            <br />

            <p>
                <asp:Button ID="btnOK" runat="server" Text="Insert" OnClick="btnOK_Click" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
            </p>

        </div>
    </form>
</body>
</html>
