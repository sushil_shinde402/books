﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Books.aspx.cs" Inherits="WebAppTest.WebPages.Books" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Books</h1>
            <asp:GridView ID ="gridBookList" runat ="server">
            </asp:GridView>
            <br />
            <asp:Button ID="btnNewBook" runat="server" Text="New Book" OnClick="btnNewBook_Click"/>
            <asp:Button ID="btnEditBook" runat="server" Text="Edit Book" OnClick="btnEditBook_Click"/>
        </div>
    </form>
</body>
</html>
